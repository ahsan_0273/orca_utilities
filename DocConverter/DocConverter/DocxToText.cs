﻿using Microsoft.Office.Interop.Word;
using System.IO;

namespace DocConverter
{
    public class DocxToTextToHtml
    {
        public static void Convert(bool isTextConversion)
        {
            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
            foreach (string fileName in Directory.GetFiles(@"E:\Work\Orca\Documents\Tesltra Source Documents\Tesltra NDA 1\Telstra Doc NDAs"))
            {
                try
                {
                    object file = fileName;
                    object readOnly = true;
                    object isVisible = false;
                    object nullobj = System.Reflection.Missing.Value;
                    Document doc = wordApp.Documents.Open(ref file, ref nullobj, ref readOnly,
                                                           ref nullobj, ref nullobj, ref nullobj,
                                                           ref nullobj, ref nullobj, ref nullobj,
                                                           ref nullobj, ref nullobj, ref nullobj);

                    string fileNameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(fileName);
                    object fileFormat = isTextConversion ? WdSaveFormat.wdFormatText : WdSaveFormat.wdFormatHTML;
                    string extension = isTextConversion ? ".txt" : ".htm";
                    string folderName = isTextConversion ? "Docx to TEXT" : "Docx To HTML";
                    object Path = @"E:\Work\Orca\Documents\Tesltra Source Documents\Tesltra NDA 1\" + folderName + @"\" + fileNameWithoutExtension + extension;
                    object oMissing = System.Reflection.Missing.Value;
                    object saveChanges = WdSaveOptions.wdDoNotSaveChanges;
                    doc.SaveAs(ref Path,
                            ref fileFormat, ref oMissing, ref oMissing,
                            ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                            ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                            ref oMissing, ref oMissing, ref oMissing, ref oMissing);
                    ((_Document)doc).Close(ref saveChanges, ref oMissing, ref oMissing);
                    doc = null;
                }
                catch (System.Exception)
                {
                    var byPass = true;
                }
                

            }
        }
    }
}
