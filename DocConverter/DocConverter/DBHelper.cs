﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocConverter
{
    public class DBHelper
    {
        public static DataTable ReadTable(string sql)
        {
            MySqlConnection con = null;
            MySqlCommand cmd = null;
            try
            {
                con = new MySqlConnection(GetConnectionString());

                cmd = con.CreateCommand();

                cmd.CommandText = sql;

                MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();

                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
                if (con != null && con.State == ConnectionState.Open)
                    con.Close();
                if (con != null)
                    con.Dispose();
            }
        }
        private static string GetConnectionString()
        {
            string connection = ConfigurationManager.ConnectionStrings["LawinsiderDB"].ToString();
            return connection;
        }
    }
}
