﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RedlineComparison.aspx.cs" Inherits="Redliner.RedlineComparison" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ORCA - Redliner</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link href="Styles/style.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            var file1, file2;
            $('#btnUploadFile').on('click', function () {
                var data = new FormData();
                var fileUpload1 = $("#fileUpload1").get(0).files;
                var fileUpload2 = $("#fileUpload2").get(0).files;
                // Add the uploaded image content to the form data collection
                if (fileUpload1.length == 0 || fileUpload2.length == 0) {
                    alert("Upload both files");
                    return;
                    
                }
                data.append("data", fileUpload1[0]);
                // Make Ajax request with the contentType = false, and procesDate = false
                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: "/api/fileupload/uploadfile",
                    contentType: false,
                    processData: false,
                    data: data
                });

                ajaxRequest.done(function (responseData, textStatus) {
                    if (textStatus == 'success') {
                        if (responseData != null) {
                            file1 = responseData;
                            $('#ifrm-one').attr('src', "Document.aspx?compare=false&fileName=" + responseData);
                            // Upload Second File

                            // Add the uploaded image content to the form data collection
                                var data = new FormData();
                                data.append("data", fileUpload2[0]);

                                // Make Ajax request with the contentType = false, and procesDate = false
                                var ajaxRequest = $.ajax({
                                    type: "POST",
                                    url: "/api/fileupload/uploadfile",
                                    contentType: false,
                                    processData: false,
                                    data: data
                                });

                                ajaxRequest.done(function (responseData, textStatus) {
                                    if (textStatus == 'success') {
                                        if (responseData != null) {
                                            file2 = responseData;
                                            $('#ifrm-two').attr('src', "Document.aspx?compare=false&fileName=" + responseData);
                                            $('#ifrm-red-liner').attr('src', "Document.aspx?compare=true&fileName1=" + file1 + "&fileName2=" + file2);
                                        }
                                    } else {
                                        alert(responseData);
                                    }
                                });
                        }
                    } else {
                        alert(responseData.Value);
                    }
                });

               

                
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table align="center" style="background-color:#9bb2c4">
                <tr>
                    <td>
                        <label for="fileUpload1">Select Original to Upload:</label>
                    </td>
                    <td>
                        <input type="file" id="fileUpload1" accept="text/html" />
                    </td>
                    <td rowspan="2"><input type="button" value="Compare" id="btnUploadFile" class ="compare-button"/></td>
                </tr>
                <tr>
                    <td>
                        <label for="fileUpload2">Select Revision to Upload:</label>
                    </td>
                    <td>
                        <input type="file" id="fileUpload2" accept="text/html"/>
                    </td>
                </tr>
            </table>
            
            <br />
                <table style="width:100%;">
            <tr>
                <tr>
                    <th>
                        <span id="doc-one">Original</span>
                    </th>
                    <th>
                        <span id="doc-two">Revision</span>
                    </th>
                    <th>
                        <span id="comparison">Redliner</span>
                    </th>
                </tr>
                <td>
                    <iframe class="ifrm"  style="width:98%;"  id="ifrm-one"></iframe>
                </td>
                <td>
                    <iframe class="ifrm" style="width:98%;" id="ifrm-two"></iframe>
                </td>
                <td>
                    <iframe class="ifrm" style="width:98%;" id="ifrm-red-liner"></iframe>
                </td>
            </tr>
        </table>
        </div>
    </form>
</body>
</html>
