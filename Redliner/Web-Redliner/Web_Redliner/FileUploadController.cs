﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace TestFileUpload
{
    public class FileUploadController : ApiController
    {
        [HttpPost]
        public string UploadFile()
        {
            try
            {

                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    string basePath = ConfigurationManager.AppSettings["BasePath"];
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = HttpContext.Current.Request.Files["data"];

                    if (httpPostedFile != null)
                    {
                        string fileName = Guid.NewGuid().ToString();
                        // Validate the uploaded image(optional)

                        // Get the complete file path
                        var fileSavePath = basePath + fileName + ".html";

                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath);

                        return fileName;
                    }

                    return "Couldn't upload file";
                }

                return "No file found to upload.";
            }
            catch (Exception ex)
            {
                return "An error occurred while uploading the file. Error Message: " + ex.Message;
            }
        }
    }
}