﻿using RedLineComparison.Redliner;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Redliner
{
    public partial class Document : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string file = string.Empty;
            try
            {
                
                string basePath = ConfigurationManager.AppSettings["BasePath"];
                bool isRedliner = bool.Parse(Request["compare"]);
                if (isRedliner == false)
                {
                    string fileName = Request["fileName"];
                    string filePath = Path.Combine(basePath + fileName + ".html");
                    file = filePath;
                    Response.Write(File.ReadAllText(filePath));
                }
                else
                {
                    string fileName1 = Request["fileName1"];
                    string fileName2 = Request["fileName2"];
                    string content1 = File.ReadAllText(basePath + fileName1 + ".html");
                    string content2 = File.ReadAllText(basePath + fileName2 + ".html");
                    string mergedContents = Comparison.PerformRedLineComparison(content1, content2);
                    Response.Write(mergedContents);
                }
            }
            catch (Exception ex)
            {
                Response.Write("Opps!! Something went wrong.. <br/>" + ex.Message+ " with " + file);
            }
            
            
            
        }
    }
}