﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RedLineComparison
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            string a = DateTime.Now.ToString("MMddyyyhhmmss");
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new UploadFiles());
        }
    }
}
