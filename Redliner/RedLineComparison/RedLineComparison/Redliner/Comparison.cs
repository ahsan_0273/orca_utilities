﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedLineComparison.Redliner
{
    public class Comparison
    {
        public static string PerformRedLineComparison(string oldContents, string newContents)
        {
            string mergedContent = string.Empty;
            string Content1 = System.Text.RegularExpressions.Regex.Replace(oldContents, @"\s*(position)\s*:\s*(relative|fixed|absolute)\s*", string.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            string Content2 = System.Text.RegularExpressions.Regex.Replace(newContents, @"\s*(position)\s*:\s*(relative|fixed|absolute)\s*", string.Empty, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Content1 = Utilities.ConvertHTMLChars(Content1);
            Content2 = Utilities.ConvertHTMLChars(Content2);
            Content1 = Utilities.RemoveTitleTags(Content1);
            Content2 = Utilities.RemoveTitleTags(Content2);
            Merger merger = new Merger(Content1, Content2);
            mergedContent = merger.merge();
            mergedContent = System.Text.RegularExpressions.Regex.Replace(mergedContent, @"((HEIGHT\s*:\s*\d+\s*px)|(WIDTH\s*:\s*\d+\s*px))", string.Empty);
            return mergedContent;

        }
    }
}
