﻿using RedLineComparison.Redliner;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RedLineComparison
{
    public partial class UploadFiles : Form
    {
        public UploadFiles()
        {
            InitializeComponent();
        }
        

        private void btnUpload_Click(object sender, EventArgs e)
        {
            Control control = (Control)sender;
            string name = control.Name;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "HTML Files(*.htm;*.html)|*.HTM;*.HTML"; // file types, that will be allowed to upload
            dialog.Multiselect = false; // allow/deny user to upload more than one file at a time
            if (dialog.ShowDialog() == DialogResult.OK) // if user clicked OK
            {
                string path = dialog.FileName; // get name of file
                if(name == "btnUpload1")
                {
                    txtFile1.Text = path;
                }
                else
                {
                    txtFile2.Text = path;
                }

            }
        }

        private void btnCompare_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if(folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                string folderPath = folderBrowserDialog.SelectedPath;
                string sourceText = File.ReadAllText(txtFile1.Text);
                string destinationText = File.ReadAllText(txtFile2.Text);
                string mergedContents = Comparison.PerformRedLineComparison(sourceText, destinationText);
                File.WriteAllText(folderPath + "\\" + "Redliner_"+ DateTime.Now.ToString("MMddyyyhhmmss") +".html", mergedContents);
            }
        }
    }
}
