﻿namespace RedLineComparison
{
    partial class UploadFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSourceFile = new System.Windows.Forms.Label();
            this.lblSecondFile = new System.Windows.Forms.Label();
            this.txtFile1 = new System.Windows.Forms.TextBox();
            this.txtFile2 = new System.Windows.Forms.TextBox();
            this.btnUpload2 = new System.Windows.Forms.Button();
            this.btnUpload1 = new System.Windows.Forms.Button();
            this.btnCompare = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblSourceFile
            // 
            this.lblSourceFile.AutoSize = true;
            this.lblSourceFile.Location = new System.Drawing.Point(12, 56);
            this.lblSourceFile.Name = "lblSourceFile";
            this.lblSourceFile.Size = new System.Drawing.Size(60, 13);
            this.lblSourceFile.TabIndex = 0;
            this.lblSourceFile.Text = "Source File";
            // 
            // lblSecondFile
            // 
            this.lblSecondFile.AutoSize = true;
            this.lblSecondFile.Location = new System.Drawing.Point(12, 103);
            this.lblSecondFile.Name = "lblSecondFile";
            this.lblSecondFile.Size = new System.Drawing.Size(63, 13);
            this.lblSecondFile.TabIndex = 1;
            this.lblSecondFile.Text = "Second File";
            // 
            // txtFile1
            // 
            this.txtFile1.Location = new System.Drawing.Point(95, 49);
            this.txtFile1.Name = "txtFile1";
            this.txtFile1.ReadOnly = true;
            this.txtFile1.Size = new System.Drawing.Size(316, 20);
            this.txtFile1.TabIndex = 2;
            // 
            // txtFile2
            // 
            this.txtFile2.Location = new System.Drawing.Point(95, 96);
            this.txtFile2.Name = "txtFile2";
            this.txtFile2.ReadOnly = true;
            this.txtFile2.Size = new System.Drawing.Size(316, 20);
            this.txtFile2.TabIndex = 3;
            // 
            // btnUpload2
            // 
            this.btnUpload2.Location = new System.Drawing.Point(435, 93);
            this.btnUpload2.Name = "btnUpload2";
            this.btnUpload2.Size = new System.Drawing.Size(75, 23);
            this.btnUpload2.TabIndex = 5;
            this.btnUpload2.Text = "Upload";
            this.btnUpload2.UseVisualStyleBackColor = true;
            this.btnUpload2.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnUpload1
            // 
            this.btnUpload1.Location = new System.Drawing.Point(435, 47);
            this.btnUpload1.Name = "btnUpload1";
            this.btnUpload1.Size = new System.Drawing.Size(75, 23);
            this.btnUpload1.TabIndex = 6;
            this.btnUpload1.Text = "Upload";
            this.btnUpload1.UseVisualStyleBackColor = true;
            this.btnUpload1.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnCompare
            // 
            this.btnCompare.ForeColor = System.Drawing.Color.Black;
            this.btnCompare.Location = new System.Drawing.Point(170, 201);
            this.btnCompare.Name = "btnCompare";
            this.btnCompare.Size = new System.Drawing.Size(139, 23);
            this.btnCompare.TabIndex = 7;
            this.btnCompare.Text = "Red liner";
            this.btnCompare.UseVisualStyleBackColor = true;
            this.btnCompare.Click += new System.EventHandler(this.btnCompare_Click);
            // 
            // UploadFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 400);
            this.Controls.Add(this.btnCompare);
            this.Controls.Add(this.btnUpload1);
            this.Controls.Add(this.btnUpload2);
            this.Controls.Add(this.txtFile2);
            this.Controls.Add(this.txtFile1);
            this.Controls.Add(this.lblSecondFile);
            this.Controls.Add(this.lblSourceFile);
            this.Name = "UploadFiles";
            this.Text = "UploadFiles";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSourceFile;
        private System.Windows.Forms.Label lblSecondFile;
        private System.Windows.Forms.TextBox txtFile1;
        private System.Windows.Forms.TextBox txtFile2;
        private System.Windows.Forms.Button btnUpload2;
        private System.Windows.Forms.Button btnUpload1;
        private System.Windows.Forms.Button btnCompare;
    }
}