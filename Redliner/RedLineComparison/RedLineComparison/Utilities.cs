﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RedLineComparison
{
    public class Utilities
    {
        public static string RemoveTitleTags(string html)
        {
            //string scriptExpression = @"<\s*style[^>]*?\s*>[\w\W]*?<\s*/style\s*>";

            string titleExpression = @"<\s*title[^>]*?\s*>[\w\W]*?<\s*/title\s*>";

            html = Regex.Replace(html, titleExpression, "", RegexOptions.IgnoreCase);

            return html;//Regex.Replace(html, scriptExpression, "", RegexOptions.IgnoreCase);
        }
        public static string ConvertHTMLChars(string input)
        {

            //General converter statement
            input = Regex.Replace(input, @"&#\d+?;", delegate (Match match)
            {
                string v = match.ToString();
                v = v.Replace("&#", "");
                v = v.Replace(";", "");
                int num = int.Parse(v);
                if ((num > 159 && num < 256) || (num > 31 && num < 127))
                {
                    v = "" + System.Convert.ToChar(num);
                    return v;
                }
                else
                {
                    return match.ToString();
                }


            }, RegexOptions.IgnoreCase);

            //put special cases below:
            input = input.Replace("&#145;", "'");
            input = input.Replace("&#146;", "'");
            input = input.Replace("&rsquo;", "'");
            input = input.Replace("&lsquo;", "'");
            input = input.Replace("&ldquo;", "\"");
            input = input.Replace("&rdquo;", "\"");
            input = input.Replace("&#147;", "\"");
            input = input.Replace("&#148;", "\"");
            input = input.Replace("&#8220;", "\"");
            input = input.Replace("&#8221;", "\"");
            input = input.Replace("&#149;", "•");
            input = input.Replace("&nbsp;", " ");
            input = input.Replace("&#183;", "·");
            input = input.Replace("&#159;", "•");
            input = input.Replace("&#151;", "—");
            input = input.Replace("&#160;", " ");
            input = input.Replace("&#038;", "&");
            input = input.Replace("&#215;", "×");
            input = input.Replace("&#216;", "Ø");
            input = input.Replace("&#133;", "...");
            input = input.Replace("&#150;", "–");
            input = input.Replace("&#8217;", "'");
            input = input.Replace("&#8226;", "•");
            input = input.Replace("&#167;", "§");
            input = input.Replace("&amp;", "&");
            input = input.Replace("&bull;", "•");
            input = input.Replace("’", "'");
            input = input.Replace("‘", "'");
            input = input.Replace("&quot;", "\"");
            input = input.Replace("&trade;", "™");
            input = input.Replace("&rsaquo;", "›");
            input = input.Replace("&lsaquo;", "‹");
            input = input.Replace("&dagger;", "†");
            input = input.Replace("&bdquo;", "„");
            input = input.Replace("&frasl;", "/");
            input = input.Replace("&lt;", "<");
            input = input.Replace("&gt;", ">");
            input = input.Replace("&ndash;", "–");
            input = input.Replace("&cent;", "¢");
            input = input.Replace("&pound;", "£");
            input = input.Replace("&yen;", "¥");
            input = input.Replace("&sect;", "§");
            input = input.Replace("&uml;", "¨");
            input = input.Replace("&die;", "¨");
            input = input.Replace("&copy;", "©");
            input = input.Replace("&reg;", "®");
            input = input.Replace("&sup2;", "²");
            input = input.Replace("&sup3;", "³");
            input = input.Replace("&middot;", "·");
            input = input.Replace("&euro;", "€");
            input = input.Replace("&brvbar;", "¦");
            input = input.Replace("&#8230;", "...");
            input = input.Replace("&#09;", " ");
            
            return input;

        }
    }
}
