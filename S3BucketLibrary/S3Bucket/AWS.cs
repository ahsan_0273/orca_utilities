﻿using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace Bucket
{
    public static class AWS
    {
        public static AWSCredentials GetAwsCredentials()
        {
            string AwsProfileLocation = ConfigurationManager.AppSettings["AwsProfileLocation"].ToString();
            string AwsProfileName = ConfigurationManager.AppSettings["AwsProfileName"].ToString();
            AWSCredentials awsCredentials;
            var chain = new CredentialProfileStoreChain(AwsProfileLocation);
            if (chain.TryGetAWSCredentials(AwsProfileName, out awsCredentials))
            {
                return awsCredentials;
            }
            return null;
        }
        public static string writeFilesList(string bucketName, string prefix, string htmlFilePath)
        {
            try
            {
                AWSCredentials awsCredentials = AWS.GetAwsCredentials();
                if (awsCredentials != null)
                {
                    var awsClient = new AmazonS3Client(awsCredentials, Amazon.RegionEndpoint.USEast1);
                    //string supportedExtensions = "*.html,*htm";
                    //  if (Directory.GetFiles(htmlFilePath, "*.*", SearchOption.AllDirectories).Where(s => supportedExtensions.Contains(Path.GetExtension(s).ToLower())).Count() > 0)
                    if (Directory.GetFiles(htmlFilePath).Count() > 0)
                    {
                        foreach (string fileName in Directory.GetFiles(htmlFilePath))
                        {
                            string html = File.ReadAllText(fileName);
                            byte[] byteArray = Encoding.UTF8.GetBytes(html);
                            string fileKey;
                            if (prefix != "" || prefix != null)
                            {
                                fileKey = string.Format("{0}/{1}", prefix, Path.GetFileName(fileName));
                            }
                            else
                            {
                                fileKey = string.Format("{0}", Path.GetFileName(fileName));
                            }
                            Stream stream = new MemoryStream(byteArray);
                            PutObjectRequest putJsonString = new PutObjectRequest
                            {
                                BucketName = bucketName,
                                Key = fileKey,
                                InputStream = stream,
                                ContentType = "text/html;charset=UTF-8"
                            };
                            PutObjectResponse putResponse = awsClient.PutObject(putJsonString);
                        }
                    }
                }
                return "done";
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                return amazonS3Exception.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string readFileList(string bucketName, string prefix, string savePath)
        {
            try
            {
                AWSCredentials awsCredentials = AWS.GetAwsCredentials();
                if (awsCredentials != null)
                {
                    var awsClient = new AmazonS3Client(awsCredentials, Amazon.RegionEndpoint.USEast1);
                    ListObjectsRequest request = new ListObjectsRequest
                    {
                        BucketName = bucketName,
                        Prefix = prefix,
                    };
                    do
                    {
                        ListObjectsResponse response = awsClient.ListObjects(request);
                        foreach (S3Object obj in response.S3Objects)
                        {
                            byte[] fileBytes = null;
                            if (!obj.Key.EndsWith("/"))
                            {
                                GetObjectRequest getObjectRequest = new GetObjectRequest
                                {
                                    BucketName = bucketName,
                                    Key = obj.Key
                                };
                                using (GetObjectResponse fileResponse = awsClient.GetObject(getObjectRequest))
                                {
                                    using (Stream responseStream = fileResponse.ResponseStream)
                                    {
                                        using (MemoryStream ms = new MemoryStream())
                                        {
                                            responseStream.CopyTo(ms);
                                            fileBytes = ms.ToArray();
                                            string html = Encoding.UTF8.GetString(fileBytes);
                                            string[] word = obj.Key.Split('/');
                                            File.WriteAllText(string.Format("{0}/{1}", savePath, word.Last()), html);
                                        }
                                    }
                                }
                            }
                        }
                        if (response.IsTruncated)
                        {
                            request.Marker = response.NextMarker;
                        }
                        else
                        {
                            request = null;
                        }
                    } while (request != null);
                }
                return "done";
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                return amazonS3Exception.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static List<string> readFileKeyList(string bucketName, string prefix)
        {
            List<string> list = new List<string>();
            AWSCredentials awsCredentials = AWS.GetAwsCredentials();
            if (awsCredentials != null)
            {
                var awsClient = new AmazonS3Client(awsCredentials, Amazon.RegionEndpoint.USEast1);
                ListObjectsRequest request = new ListObjectsRequest
                {
                    BucketName = bucketName,
                    Prefix = prefix,
                };
                do
                {
                    ListObjectsResponse response = awsClient.ListObjects(request);
                    foreach (S3Object obj in response.S3Objects)
                    {
                        if (!obj.Key.EndsWith("/"))
                        {
                            string[] word = obj.Key.Split('/');
                            if (word.Count() > 1)
                            {
                                list.Add(word.Last());
                            }
                            else
                            {
                                list.Add(word[0]);
                            }
                        }
                    }
                    if (response.IsTruncated)
                    {
                        request.Marker = response.NextMarker;
                    }
                    else
                    {
                        request = null;
                    }
                } while (request != null);
            }
            return list;
        }
        public static string readFile(string bucketName, string prefix, string fileName, string savePath)
        {
            try
            {
                AWSCredentials awsCredentials = AWS.GetAwsCredentials();
                if (awsCredentials != null)
                {
                    var awsClient = new AmazonS3Client(awsCredentials, Amazon.RegionEndpoint.USEast1);

                    if (prefix != "" || prefix != null)
                    {
                        fileName = string.Format("{0}/{1}", prefix, fileName);
                    }
                    GetObjectRequest getObjectRequest = new GetObjectRequest
                    {
                        BucketName = bucketName,
                        Key = fileName
                    };

                    byte[] fileBytes;
                    using (GetObjectResponse fileResponse = awsClient.GetObject(getObjectRequest))
                    {
                        using (Stream responseStream = fileResponse.ResponseStream)
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                responseStream.CopyTo(ms);
                                fileBytes = ms.ToArray();
                                string html = Encoding.UTF8.GetString(fileBytes);
                                File.WriteAllText(string.Format("{0}/{1}", savePath, fileName), html);
                            }
                        }
                    }
                }
                return "done";
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                return amazonS3Exception.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string writeFile(string bucketName, string prefix, string filePath)
        {
            try
            {
                AWSCredentials awsCredentials = AWS.GetAwsCredentials();

                if (awsCredentials != null)
                {
                    var awsClient = new AmazonS3Client(awsCredentials, Amazon.RegionEndpoint.USEast1);
                    string data = File.ReadAllText(filePath);
                    byte[] byteArray = Encoding.UTF8.GetBytes(data);
                    string fileKey = Path.GetFileName(filePath);
                    if (prefix != "" || prefix != null)
                    {
                        fileKey = string.Format("{0}/{1}", prefix, fileKey);
                    }
                    Stream stream = new MemoryStream(byteArray);
                    PutObjectRequest putJsonString = new PutObjectRequest
                    {
                        BucketName = bucketName,
                        Key = fileKey,
                        InputStream = stream,
                        ContentType = "text/html;charset=UTF-8"
                    };
                    PutObjectResponse putResponse = awsClient.PutObject(putJsonString);
                }
                return "done";
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                return amazonS3Exception.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string deleteListFile(string bucketName, string prefix)
        {
            try
            {
                AWSCredentials awsCredentials = AWS.GetAwsCredentials();
                if (awsCredentials != null)
                {
                    var awsClient = new AmazonS3Client(awsCredentials, Amazon.RegionEndpoint.USEast1);
                    ListObjectsRequest request = new ListObjectsRequest
                    {
                        BucketName = bucketName,
                        Prefix = prefix
                    };
                    do
                    {
                        ListObjectsResponse response = awsClient.ListObjects(request);
                        foreach (S3Object obj in response.S3Objects)
                        {
                            if (!obj.Key.EndsWith("/"))
                            {
                                DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                                deleteRequest.BucketName = bucketName;
                                deleteRequest.Key = obj.Key;

                                awsClient.DeleteObject(deleteRequest);
                            }
                        }
                        if (response.IsTruncated)
                        {
                            request.Marker = response.NextMarker;
                        }
                        else
                        {
                            request = null;
                        }
                    } while (request != null);

                    return "done";
                }
                else
                {
                    return "unable to delete";
                }
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                return amazonS3Exception.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
        public static string deleteFile(string bucketName, string prefix, string fileKey)
        {
            try
            {
                AWSCredentials awsCredentials = AWS.GetAwsCredentials();
                if (awsCredentials != null)
                {
                    var awsClient = new AmazonS3Client(awsCredentials, Amazon.RegionEndpoint.USEast1);
                    if (prefix != "" || prefix != null)
                    {
                        fileKey = string.Format("{0}/{1}", prefix, fileKey);
                    }
                    DeleteObjectRequest deleteRequest = new DeleteObjectRequest();
                    deleteRequest.BucketName = bucketName;
                    deleteRequest.Key = fileKey;
                    awsClient.DeleteObject(deleteRequest);
                    return "done";
                }
                return "notdelete";
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                return amazonS3Exception.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }
    }
}
