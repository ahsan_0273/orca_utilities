﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Document_Stripper
{
    public class Patterns
    {
        public static string validOrgPattern = @"(?:^|\s)(?:Corp|Inc|Engineers|Company|LTD|clc|Association|s.c.|& co|clc|board|PLLC|LLC|LC|BANK|LLP|LP|P\.C)\.?$";
        public static Regex validOrgRegex = new Regex(validOrgPattern);
       
    }
}
