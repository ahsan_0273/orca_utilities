﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Document_Stripper
{
    public class Constants
    {
        public static string Organization = "ORGANIZATION";
        public static string Location = "Location";
        public static string Person = "Person";
        public static string Date = "Date";
    }
}
