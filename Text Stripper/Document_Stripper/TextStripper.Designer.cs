﻿namespace Document_Stripper
{
    partial class TextStripper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBoxReadPath = new System.Windows.Forms.TextBox();
            this.DirectoryPath = new System.Windows.Forms.Label();
            this.btnStartStripping = new System.Windows.Forms.Button();
            this.labelToolName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtBoxReadPath
            // 
            this.txtBoxReadPath.Location = new System.Drawing.Point(159, 98);
            this.txtBoxReadPath.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBoxReadPath.Name = "txtBoxReadPath";
            this.txtBoxReadPath.Size = new System.Drawing.Size(263, 22);
            this.txtBoxReadPath.TabIndex = 0;
            // 
            // DirectoryPath
            // 
            this.DirectoryPath.AutoSize = true;
            this.DirectoryPath.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DirectoryPath.Location = new System.Drawing.Point(31, 102);
            this.DirectoryPath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DirectoryPath.Name = "DirectoryPath";
            this.DirectoryPath.Size = new System.Drawing.Size(114, 17);
            this.DirectoryPath.TabIndex = 1;
            this.DirectoryPath.Text = "Directory Path";
            // 
            // btnStartStripping
            // 
            this.btnStartStripping.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartStripping.Location = new System.Drawing.Point(199, 154);
            this.btnStartStripping.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnStartStripping.Name = "btnStartStripping";
            this.btnStartStripping.Size = new System.Drawing.Size(168, 28);
            this.btnStartStripping.TabIndex = 2;
            this.btnStartStripping.Text = "Start Stripping";
            this.btnStartStripping.UseVisualStyleBackColor = true;
            this.btnStartStripping.Click += new System.EventHandler(this.buttonStartStripping_Click);
            // 
            // labelToolName
            // 
            this.labelToolName.AutoSize = true;
            this.labelToolName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelToolName.Location = new System.Drawing.Point(139, 27);
            this.labelToolName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelToolName.Name = "labelToolName";
            this.labelToolName.Size = new System.Drawing.Size(299, 28);
            this.labelToolName.TabIndex = 3;
            this.labelToolName.Text = "Document Stripping Tool";
            // 
            // TextStripper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 377);
            this.Controls.Add(this.labelToolName);
            this.Controls.Add(this.btnStartStripping);
            this.Controls.Add(this.DirectoryPath);
            this.Controls.Add(this.txtBoxReadPath);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "TextStripper";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBoxReadPath;
        private System.Windows.Forms.Label DirectoryPath;
        private System.Windows.Forms.Button btnStartStripping;
        private System.Windows.Forms.Label labelToolName;
    }
}

