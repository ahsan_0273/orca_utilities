﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Document_Stripper
{
    class PersonNameExtraction
    {
        public static List<string> GetPersonNames(string text, object[] classifiedNerResult)
        {
            List<string> extractedPersons = new List<string>();
            var nerExtractedPersons = DataExtractionUtilities.GetNERResults(Constants.Person, classifiedNerResult);
            for (int i = 0; i < nerExtractedPersons.Count; i++)
            {
                extractedPersons.Add(DataExtractionUtilities.GetNameFromIndexes(text, nerExtractedPersons[i]));
            }
            return DataExtractionUtilities.GetDistinctOrderedList(extractedPersons);
        }
    }
}
