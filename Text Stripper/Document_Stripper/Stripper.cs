﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;
using System.IO;
using Word = Microsoft.Office.Interop.Word;
using System.Windows.Forms;

namespace Document_Stripper
{
    class Stripper
    {
        Classifier _classifier = new Classifier();
        
        public void StartStripping(string directoryPath)
        {
            //iterate through all file, present in specified directory
            foreach (string filePath in Directory.GetFiles(directoryPath, "*.docx"))
            {
                string outputDirectory = directoryPath + "\\output\\";
                if (!Directory.Exists(outputDirectory))
                    Directory.CreateDirectory(outputDirectory);
                string outputFilePath = outputDirectory + Path.GetFileName(filePath);
                ReadAndReplaceDocument(filePath, outputFilePath);
            }
            MessageBox.Show("Documnet Stripping Completed");

        }
        //this function reads a document, makes a copy of it and replace NER with placeholders and save copy
        public void ReadAndReplaceDocument(string filePath, string outputFilePath)
        {
            Document documentToBeReplaced = DocumentProcessor.ReadFile(filePath);

            string extractedDocText = documentToBeReplaced.Content.Text.Trim();// extracting text of orignal document for ner classifier

            List<string> textToBeReplaced = GetTextToBeReplaced(extractedDocText);

            DocumentProcessor.StartReplacement(textToBeReplaced, documentToBeReplaced);
            
            DocumentProcessor.SaveReplacedDocument(documentToBeReplaced, outputFilePath);
        }

        private List<string> GetTextToBeReplaced(string extractedDocText)
        {
            List<string> textToBeReplaced = new List<string>();
            // Get NER extracted results
            object[] classifiedNerResult = _classifier.GetNERExtractedResults(extractedDocText);

            //extract valid organization names after regex filtering
            textToBeReplaced.AddRange(OrganizationNameExtraction.GetOrganizationNames(extractedDocText, "", classifiedNerResult));
            textToBeReplaced.AddRange(PersonNameExtraction.GetPersonNames(extractedDocText, classifiedNerResult));
            textToBeReplaced.AddRange(DateExtraction.GetDates(extractedDocText, classifiedNerResult));
            textToBeReplaced.AddRange(LocationExtraction.GetLocations(extractedDocText, classifiedNerResult));

            return textToBeReplaced;
        }
    }
}
