﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Document_Stripper
{
    class DataExtractionUtilities
    {
        public static string GetNameFromIndexes(string text, object nerResult)
        {
            int startindex = 0;
            int endIndex = 0;

            int.TryParse(((edu.stanford.nlp.util.Triple)(nerResult)).second().ToString(), out startindex);
            int.TryParse(((edu.stanford.nlp.util.Triple)(nerResult)).third().ToString(), out endIndex);
            return text.Substring(startindex, endIndex - startindex);
        }

        public static List<object> GetNERResults(string token, object[] classifiedNerResult)
        {
            return classifiedNerResult.Where(obj => string.Equals(((edu.stanford.nlp.util.Triple)(obj)).first().ToString(), token, StringComparison.OrdinalIgnoreCase)).ToList();
        }

        public static List<string> GetDistinctOrderedList(List<string> nerExtractedResults)
        {
            List<string> distinctOrderedResults = new List<string>();
            distinctOrderedResults = nerExtractedResults.Distinct().OrderByDescending(val => val)
                                .ThenByDescending(val => val.Length).ToList();
            return distinctOrderedResults;
        }
    }
}
