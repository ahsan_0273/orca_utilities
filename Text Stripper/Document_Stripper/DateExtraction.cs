﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Document_Stripper
{
    class DateExtraction
    {
        public static List<string> GetDates(string text, object[] classifiedNerResult)
        {
            List<string> extractedDates = new List<string>();
            var nerExtractedDates = DataExtractionUtilities.GetNERResults(Constants.Date, classifiedNerResult);
            for (int i = 0; i < nerExtractedDates.Count; i++)
            {
                extractedDates.Add(DataExtractionUtilities.GetNameFromIndexes(text, nerExtractedDates[i]));
            }
            return DataExtractionUtilities.GetDistinctOrderedList(extractedDates);
        }
    }
}
