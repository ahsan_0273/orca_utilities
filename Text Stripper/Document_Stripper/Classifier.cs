﻿using edu.stanford.nlp.ie.crf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace Document_Stripper
{
    public class Classifier
    {
        public CRFClassifier classifier = null;
        public Classifier()
        {
            var appSettings = ConfigurationManager.AppSettings;
            // Loading 7 class classifier model
            classifier = CRFClassifier.getClassifierNoExceptions(
                appSettings["ClassifierDirectory"] + appSettings["Model"]);
        }

        public object[] GetNERExtractedResults(string text)
        {
            return classifier.classifyToCharacterOffsets(text).toArray();
        }
    }
}
