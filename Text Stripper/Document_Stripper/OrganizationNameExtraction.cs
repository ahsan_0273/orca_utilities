﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;

namespace Document_Stripper
{
    class OrganizationNameExtraction
    {
        public static  List<string> GetOrganizationNames(string text, string fileName, object[] classifiedNerResult)
        {
            List<string> extratedNerNamesList = ExtractNEROrgName(text, fileName, classifiedNerResult);
            return ExtractValidOrgName(extratedNerNamesList, text);
        }

        static List<string> ExtractNEROrgName(string text, string fileName, object[] classifiedNerResults)
        {
            List<string> extractedOrganizations = new List<string>();
            var nerExtractedOrg = DataExtractionUtilities.GetNERResults(Constants.Organization, classifiedNerResults);
            for (int i = 0; i < nerExtractedOrg.Count; i++)
            {
                extractedOrganizations.Add(DataExtractionUtilities.GetNameFromIndexes(text, nerExtractedOrg[i]));
            }
            return extractedOrganizations.Distinct().ToList();

            #region [Obsolete]
            // Storing NER results in below variables
            string nerTag = string.Empty;
            int startindex = 0;
            int endIndex = 0;
            List<string> nerOrgNamesList = new List<string>();
            //Sidra

            if (classifiedNerResults != null && classifiedNerResults.Any())
            {
                for (int i = 0; i < classifiedNerResults.Length; i++)
                {
                    nerTag = ((edu.stanford.nlp.util.Triple)(classifiedNerResults[i])).first().ToString();
                    int.TryParse(((edu.stanford.nlp.util.Triple)(classifiedNerResults[i])).second().ToString(), out startindex);
                    int.TryParse(((edu.stanford.nlp.util.Triple)(classifiedNerResults[i])).third().ToString(), out endIndex);

                    if (nerTag.Equals(Constants.Organization, StringComparison.OrdinalIgnoreCase))
                    {
                        string nerOrgName = text.Substring(startindex, endIndex - startindex);

                        // Maintaining Unique list of NER organizations
                        if (!nerOrgNamesList.Contains(nerOrgName))
                        {
                            nerOrgNamesList.Add(nerOrgName);
                            File.AppendAllText(@"NER Results/" + fileName + "_Allorg.txt", nerOrgName + Environment.NewLine);
                        }
                    }
                }
            }
            
            return nerOrgNamesList;
            #endregion
        }
        static List<string> ExtractValidOrgName(List<string> nerExtractedOrgNames, string text)
        {
            List<string> validOrgNames = new List<string>();
            foreach (string nerOrgName in nerExtractedOrgNames)
            {
                
                Regex regex = new Regex(@"(?:^|\s)(?:Corp|Inc|Engineers|Company|LTD|clc|Association|s.c.|& co|clc|board|PLLC|LLC|LC|BANK|LLP|LP|P\.C)\.?$", RegexOptions.IgnoreCase);
                Match match = regex.Match(nerOrgName);
                if (match.Success)
                {
                    string validOrgName = nerOrgName;
                    int wordCount = nerOrgName.Trim().Split(' ').Count();
                    if (wordCount <= 1)
                        continue;
                    if (nerOrgName.Contains("\n"))
                    {
                        validOrgName = nerOrgName.Substring(nerOrgName.LastIndexOf("\n") + 1);
                    }
                    if (!validOrgNames.Contains(validOrgName))
                    {
                        validOrgNames.Add(validOrgName);
                    }
                   
                    text = Regex.Replace(text, @"\“", "\"");
                    text = Regex.Replace(text, @"\”", "\"");
                    Regex regexAbbrivation = new Regex(validOrgName + "\\s*\\(\"\\s*(?<alias>\\S+\\s*)\"\\)", RegexOptions.IgnoreCase);
                    Match matchAbbrivation = regexAbbrivation.Match(text);
                    if (matchAbbrivation.Success)
                    {
                        if (!validOrgNames.Contains(matchAbbrivation.Groups["alias"].Value))
                        {
                            validOrgNames.Add(matchAbbrivation.Groups["alias"].Value);
                        }
                    }
                    // TODO: Revies this logic
                    string trimedValidOrgName;
                    if (validOrgName.LastIndexOf(' ') > -1)
                    {
                        trimedValidOrgName = validOrgName.Remove(validOrgName.LastIndexOf(' '));
                    }
                    else
                    {
                        trimedValidOrgName = validOrgName;
                    }

                    foreach (string orgName in nerExtractedOrgNames)
                    {
                        Regex wordBoundaryRegex = new Regex("\\b(" + orgName + ")\\b", RegexOptions.IgnoreCase);
                        if (wordBoundaryRegex.IsMatch(trimedValidOrgName))
                        {
                            if (!validOrgNames.Contains(orgName))
                            {
                                validOrgNames.Add(orgName);
                            }
                        }
                    }

                }
            }
            return DataExtractionUtilities.GetDistinctOrderedList(validOrgNames);
        }
    }
}
