﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Document_Stripper
{
    class LocationExtraction
    {
        public static List<string> GetLocations(string text, object[] classifiedNerResult)
        {
            List<string> extractedLocations = new List<string>();
            var nerExtractedLocations = DataExtractionUtilities.GetNERResults(Constants.Location, classifiedNerResult);
            for (int i = 0; i < nerExtractedLocations.Count; i++)
            {
                extractedLocations.Add(DataExtractionUtilities.GetNameFromIndexes(text, nerExtractedLocations[i]));
            }
            return DataExtractionUtilities.GetDistinctOrderedList(extractedLocations);
        }
    }
}
