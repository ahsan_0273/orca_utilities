﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;
using Word = Microsoft.Office.Interop.Word;

namespace Document_Stripper
{
    class DocumentProcessor
    {

        internal static void StartReplacement(List<string> textToBeReplaced, Document documentToBeReplaced)
        {
            //replacing Recognized named entities with placeholders
            foreach (string orgName in textToBeReplaced)
            {
                SearchAndReplaceEverywhere(documentToBeReplaced, orgName, "[PLACEHOLDERS]");
            }

        }

        internal static Document ReadFile(string filePath)
        {
            Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();
            object file = filePath;
            object readOnly = false;
            object isVisible = false;
            object nullobj = System.Reflection.Missing.Value;

            Document doc = wordApp.Documents.Open(ref file, ref nullobj, ref readOnly,
                                                   ref nullobj, ref nullobj, ref nullobj,
                                                   ref nullobj, ref nullobj, ref nullobj,
                                                   ref nullobj, ref nullobj, ref nullobj);

            Document replacedDocument = CopyToNewDocument(doc);// making copy of orignal document
            doc.Close();
            return replacedDocument;
            
        }

        public static Word.Document CopyToNewDocument(Word.Document document)
        {
            document.StoryRanges[Word.WdStoryType.wdMainTextStory].Copy();

            var newDocument = document.Application.Documents.Add();
            newDocument.StoryRanges[Word.WdStoryType.wdMainTextStory].Paste();
            return newDocument;
        }

        public static void SearchAndReplaceEverywhere(
          Word.Document document, string find, string replace)
        {
            foreach (Word.Range storyRange in document.StoryRanges)
            {
                var range = storyRange;
                while (range != null)
                {
                    SearchAndReplaceInStoryRange(range, find, replace);

                    if (range.ShapeRange.Count > 0)
                    {
                        foreach (Word.Shape shape in range.ShapeRange)
                        {
                            if (shape.TextFrame.HasText != 0)
                            {
                                SearchAndReplaceInStoryRange(
                                    shape.TextFrame.TextRange, find, replace);
                            }
                        }
                    }
                    range = range.NextStoryRange;
                }
            }
        }

        internal static void SaveReplacedDocument(Document replacedDocument, string outputFilePath)
        {
            replacedDocument.SaveAs2(outputFilePath);
            replacedDocument.Close();
        }

        public static void SearchAndReplaceInStoryRange(
            Word.Range range, string find, string replace)
        {
            range.Find.ClearFormatting();
            range.Find.Replacement.ClearFormatting();
            range.Find.Text = find;
            range.Find.Replacement.Text = replace;
            range.Find.Wrap = Word.WdFindWrap.wdFindContinue;
            range.Find.Execute(Replace: Word.WdReplace.wdReplaceAll);
        }
    }
}
