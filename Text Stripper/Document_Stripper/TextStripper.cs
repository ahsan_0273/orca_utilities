﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;

namespace Document_Stripper
{
    public partial class TextStripper : Form
    {
        
        Stripper DocStripper = new Stripper();
        public TextStripper()
        {
            InitializeComponent();
        }

        private void buttonStartStripping_Click(object sender, EventArgs e)
        {
            string directoryPath = txtBoxReadPath.Text;
            if (!Directory.Exists(directoryPath))
                MessageBox.Show("In Valid Directory Path");
            else
                DocStripper.StartStripping(directoryPath);


        }
    }
}
