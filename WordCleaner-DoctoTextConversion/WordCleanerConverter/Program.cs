﻿using System;
using System.IO;
using System.Linq;

namespace WordCleanerConverter
{
    public class Program
    {
        private static readonly string DOC_TO_HTML = "doc_to_html";
        private static readonly string DOCX_TO_HTML = "docx_to_html";
        private static readonly string PDF_TO_HTML = "pdf_to_html";
        private static readonly string HTML_TO_DOC = "html_to_doc";
        private static readonly string HTML_TO_DOCX = "html_to_docx";
        private static readonly string HTML_TO_PDF = "html_to_pdf";
        private static readonly string GENERATE_FILE = "--generate";
        
        /* *********************************************************
         * Total command line arguments (for document conversion): 3
         * The first command line argument must be the filename to convert.
         * The second command line argument must be the filename to convert to.
         * Third argument can be one of the strings: "doc_to_html", "docx_to_html", "pdf_to_html", "html_to_doc", "html_to_docx", "html_to_pdf"
         * **********************************************************
         * For activation file generation: pass a flag "--generate"
         * And the next parameter would be the path of the directory where to generate activation file
         * **********************************************************
         */
        public static void Main(string[] args)
        {
            if (args.Length == 2) // might be for activation file generation
            {
                string sourcePath = args[0];
                string destinationPath = args[1];
                Converter converter = new Converter();
                bool isConverted = converter.ConvertDocumentToText(sourcePath, destinationPath);
                if(!isConverted)
                {
                    Environment.Exit(1);
                    return;
                }
                //if (flag.Equals(GENERATE_FILE))
                //{
                //    FileAttributes attr = File.GetAttributes(path);
                //    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                //    {
                //        WordCleanerApi.WordCleaner.GenerateActivationFile(path);
                //    }
                //    else
                //    {
                //        Console.Error.WriteLine("Invalid path.");
                //        Environment.Exit(1);
                //        return;
                //    }
                //}
                //else
                //{
                //    Console.Error.WriteLine("Invalid parameters.");
                //    Environment.Exit(1);
                //}
            }

            // For conversion
            //int allowedArguments = 3;
            //if (args.Length != allowedArguments)
            //{
            //    Environment.Exit(1);
            //    return;
            //}
            //string sourceFile = args[0];
            //string destinationFile = args[1];
            //string decisionParameter = args[2].ToLower();

            //if (!VerifyDecisionParameter(decisionParameter))
            //{
            //    Environment.Exit(1);
            //    return;
            //}

            //int firstUnderscore = decisionParameter.IndexOf('_'), secondUnderscore = decisionParameter.LastIndexOf('_');
            //string actualSourceFileExt = decisionParameter.Substring(0, firstUnderscore),
            //       actualDestinationFileExt = decisionParameter.Substring(secondUnderscore + 1);

            //if (sourceFile.Length < (actualSourceFileExt.Length + 2) || destinationFile.Length < (actualDestinationFileExt.Length + 2))
            //{
            //    Environment.Exit(1);
            //    return;
            //}

            //string sourceFileExt = sourceFile.Substring(sourceFile.Length - (actualSourceFileExt.Length + 1)).ToLower(),
            //       destinationFileExt = destinationFile.Substring(destinationFile.Length - (actualDestinationFileExt.Length + 1)).ToLower();
            //if (!sourceFileExt.Equals("." + actualSourceFileExt) || !destinationFileExt.Equals("." + actualDestinationFileExt))
            //{
            //    Environment.Exit(1);
            //    return;
            //}

            //Converter converter = new Converter();
            //bool converted = false;
            //if (actualSourceFileExt.Equals("html"))
            //    converted = converter.ConvertHTMLToDocument(sourceFile, destinationFile, actualDestinationFileExt);
            //else
            //    converted = converter.ConvertDocumentToHTML(sourceFile, destinationFile, actualSourceFileExt);

            //if (!converted)
            //{
            //    Environment.Exit(1);
            //    return;
            //}
        }
        
        private static bool VerifyDecisionParameter(string decisionParameter)
        {
            return 
                (decisionParameter.Equals(DOC_TO_HTML) || decisionParameter.Equals(DOCX_TO_HTML) ||
                decisionParameter.Equals(PDF_TO_HTML) || decisionParameter.Equals(HTML_TO_DOC) ||
                decisionParameter.Equals(HTML_TO_DOCX) || decisionParameter.Equals(HTML_TO_PDF));
        }
    }
}
