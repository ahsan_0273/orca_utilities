﻿using System;
using System.Configuration;
using System.Reflection;
using log4net;

namespace WordCleanerConverter
{
    /*
     * Provides a wrapper for the word cleaner API to convert word documents to HTML, and HTML back to word docuements.
     */
    public class Converter
    {
        private WordCleanerApi.WordCleaner wordCleaner = null;
        private readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public Converter()
        {
            WordCleanerApi.WordCleaner.ActivateConvertingEngine();
            wordCleaner = new WordCleanerApi.WordCleaner(
                ConfigurationManager.AppSettings["WordCleanerSerial"],
                ConfigurationManager.AppSettings["WordCleanerEmail"],
                ConfigurationManager.AppSettings["WordCleanerCode"]
            );
            wordCleaner.LogToFile = true;
        }
        public bool ConvertDocumentToText(string sourcePath, string destinationPath)
        {
            string wordCleanerTemplate = "";
            string wordCleanerTemplatePath = ConfigurationManager.AppSettings["WordCleanerTemplatePath"];
            try
            {
                wordCleanerTemplate = ConfigurationManager.AppSettings["WordCleanerConvertToTextTemplate"];
                bool isConverted = wordCleaner.ProcessDocumentWithTemplate(
                    sourcePath,
                    destinationPath,
                    wordCleanerTemplatePath + wordCleanerTemplate
                );
                return isConverted;
                
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        /*
         * Parameters:
         *      sourcePath: path of the source file
         *      destinationPath: path of the destination file
         *      documentType: type of source document (doc/docx/pdf)
         *      
         * Converts the file at sourcePath to HTML and saves to destinationPath
         */
        public bool ConvertDocumentToHTML(string sourcePath, string destinationPath, string documentType)
        {
            string wordCleanerTemplate = "";
            string wordCleanerTemplatePath = ConfigurationManager.AppSettings["WordCleanerTemplatePath"];
            try
            {
                if (!documentType.ToLower().Equals("pdf"))
                    wordCleanerTemplate = ConfigurationManager.AppSettings["WordCleanerConvertToHTMLTemplate"];
                else
                    wordCleanerTemplate = ConfigurationManager.AppSettings["WordCleanerConvertPDFToHTMLTemplate"];

                bool isConverted = wordCleaner.ProcessDocumentWithTemplate(
                    sourcePath,
                    destinationPath,
                    wordCleanerTemplatePath + wordCleanerTemplate
                );
                if (!isConverted)
                {
                    string errors = "";
                    foreach (string error in wordCleaner.Errors)
                        errors += (error + ";");
                    log.Error(errors);
                    Console.Error.WriteLine("Errors Occurred: " + errors + 
                        "\nsourcePath: " + sourcePath + 
                        "\ndestinationPath: " + destinationPath + 
                        "\ndocumentType: " + documentType + 
                        "\nwordCleanerTemplate: " + wordCleanerTemplate + 
                        "\nwordCleanerTemplatePath: " + wordCleanerTemplatePath);
                }
                return isConverted;
            }
            catch (Exception ex)
            {
                log.Error("Exception: ", ex);
                Console.Error.WriteLine("Errors Occurred: " + ex + 
                    "\nsourcePath: " + sourcePath + 
                    "\ndestinationPath: " + destinationPath + 
                    "\ndocumentType: " + documentType + 
                    "\nwordCleanerTemplate: " + wordCleanerTemplate + 
                    "\nwordCleanerTemplatePath: " + wordCleanerTemplatePath);
                return false;
            }
        }

        /*
         * Parameters:
         *      sourcePath: path of the source file
         *      destinationPath: path of the destination file
         *      documentType: type of destination document (doc/docx/pdf)
         *      
         * Converts the file at sourcePath to specified document and saves to destinationPath
         */
        public bool ConvertHTMLToDocument(string sourcePath, string destinationPath, string documentType)
        {
            try
            {
                switch (documentType.ToLower())
                {
                    case "doc":
                        wordCleaner.ConvertFileToDoc(sourcePath, destinationPath);
                        break;
                    case "docx":
                        wordCleaner.ConvertFileToDocx(sourcePath, destinationPath);
                        break;
                    case "pdf":
                        wordCleaner.ConvertFileToPdf(sourcePath, destinationPath);
                        break;
                    default:
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                log.Error("Exception: ", ex);
                Console.Error.WriteLine("Errors Occurred: " + ex + 
                    "\nsourcePath: " + sourcePath + 
                    "\ndestinationPath: " + destinationPath + 
                    "\ndocumentType: " + documentType);
                return false;
            }
        }
    }
}
