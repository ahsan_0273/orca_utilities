﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Web_DocToTextConverter
{
    public class FileUploadController : ApiController
    {
        [HttpPost]
        public string UploadFile()
        {
            try
            {

                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    string basePath = ConfigurationManager.AppSettings["BasePath"];
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = HttpContext.Current.Request.Files["data"];

                    if (httpPostedFile != null)
                    {
                        string fileName = Guid.NewGuid().ToString();
                        // Validate the uploaded image(optional)
                        string extension = string.Empty;

                        extension = System.IO.Path.GetExtension(httpPostedFile.FileName).ToString();

                        // Get the complete file path
                        var fileSavePath = basePath + fileName + extension;

                        // Save the uploaded file to "UploadedFiles" folder
                        httpPostedFile.SaveAs(fileSavePath);

                        return fileName + extension;
                    }

                    return "Couldn't upload file";
                }

                return "No file found to upload.";
            }
            catch (Exception ex)
            {
                throw;
                return "An error occurred while uploading the file. Error Message: " + ex.Message;
            }
        }

        public string UploadFolder()
        {
            try
            {
                int processedFileCount = 0;
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    string folderName = Guid.NewGuid().ToString();
                    string basePath = ConfigurationManager.AppSettings["BasePath"];
                    
                    foreach (string file in HttpContext.Current.Request.Files)
                    {
                        var fileContent = HttpContext.Current.Request.Files[file];
                        if (fileContent != null && fileContent.ContentLength > 0)
                        {
                            string extension = System.IO.Path.GetExtension(fileContent.FileName).ToString();
                            if (extension.Equals(".docx", StringComparison.OrdinalIgnoreCase)
                                || extension.Equals(".doc", StringComparison.OrdinalIgnoreCase))
                            {

                                processedFileCount = processedFileCount + 1;
                                string fileName = fileContent.FileName;
                                string directoryPath = basePath + "\\" + folderName + "\\" + fileName.Substring(0, fileName.LastIndexOf("/"));
                                if (!Directory.Exists(directoryPath))
                                {
                                    Directory.CreateDirectory(directoryPath);
                                }
                                string filePath = basePath + "\\" + folderName + "\\" + fileContent.FileName;
                                // get a stream
                                fileContent.SaveAs(filePath);
                            }
                        }
                    }
                    if(processedFileCount > 0)
                    {
                        return folderName;
                    }
                    else
                    {
                        throw new Exception("No .doc/.docx document present in folder.");
                    }
                    
                }

            }
            catch (Exception ex)
            {
                throw;
            }
            return "Couldn't upload folder";
        }
    }
}