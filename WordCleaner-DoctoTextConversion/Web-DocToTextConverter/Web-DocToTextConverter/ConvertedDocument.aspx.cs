﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web_DocToTextConverter
{
    public partial class ConvertedDocument : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string basePath = ConfigurationManager.AppSettings["BasePath"];
                string fileName = Request["fileName"];
                string filePath = Path.Combine(basePath + fileName);
                string destinationFileName = Guid.NewGuid().ToString();
                string destFilePath = Path.Combine(basePath + destinationFileName + ".txt");
                bool isConverted = MakeCallToWordCleanerExe(filePath, destFilePath);
                if (isConverted)
                {
                    try
                    {
                        Response.Write(File.ReadAllText(destFilePath));
                        Response.ContentType = "text/plain";
                        File.Delete(filePath);
                        File.Delete(destFilePath);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                   
                }
                else
                {
                    Response.Write("File not converted.");
                }
            }
            catch (Exception ex)
            {
                Response.Write("Opps!! Something went wrong.. <br/>" + ex.Message);
            }



        }

        private static bool MakeCallToWordCleanerExe(string sourceFile, string destinationFile)
        {
            Process wordCleanerProc = new Process();
            wordCleanerProc.StartInfo.FileName = ConfigurationManager.AppSettings["WordCleanerConverterExe"];
            wordCleanerProc.StartInfo.Arguments = string.Format(" \"{0}\" \"{1}\"", sourceFile, destinationFile);

            wordCleanerProc.StartInfo.UseShellExecute = false;
            wordCleanerProc.StartInfo.RedirectStandardError = true;
            wordCleanerProc.Start();
            string errors = wordCleanerProc.StandardError.ReadToEnd();
            if (!errors.Equals(""))
            {
                throw new Exception(errors);
            }

            wordCleanerProc.WaitForExit();
            var exitCode = wordCleanerProc.ExitCode;
            wordCleanerProc.Close();

            return (exitCode != 1);
        }
    }
}