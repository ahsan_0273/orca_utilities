﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocConverter.aspx.cs" Inherits="Web_DocToTextConverter.DocConverter" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Orca - Doc Converter</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link href="Styles/style.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            setInterval(function () {
                var documentCookies = document.cookie.split(";");
                for (i = 0; i < documentCookies.length; i++) {
                    var spcook = documentCookies[i].split("=");
                    var startIndex = documentCookies[i].indexOf("status");
                    if(startIndex != -1)
                    {
                        $(".loader").hide();
                        $(".overlay").hide();
                        clearListCookies();
                    }
                }
            }, 3000);
            function clearListCookies() {
                var cookies = document.cookie.split(";");
                for (var i = 0; i < cookies.length; i++) {
                    var spcook = cookies[i].split("=");
                    document.cookie = spcook[0] + "=;expires=Thu, 21 Sep 1979 00:00:01 UTC;";
                }
            }

            $('#ifrm-document').on("load", function () {
                $(".loader").hide();
                $(".overlay").hide();
            });
            var file_name;
            $('#btnUploadFile').on('click', function () {
                
                $('.error-message').hide();
                var data = new FormData();
                var fileUpload1 = $("#fileUpload1").get(0).files;
                // Add the uploaded image content to the form data collection
                if (fileUpload1.length == 0) {
                    $('.error-message').show();
                    $('.error-message').text("Please upload a file");
                    return;
                }
                data.append("data", fileUpload1[0]);
                $(".loader").show();
                $(".overlay").show();

                // Make Ajax request with the contentType = false, and procesDate = false
                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: "/api/fileupload/uploadfile",
                    contentType: false,
                    processData: false,
                    data: data
                });

                ajaxRequest.done(function (responseData, textStatus) {
                    if (textStatus == 'success') {
                        if (responseData != null) {
                            file_name = responseData;
                            $('#ifrm-document').attr('src', "ConvertedDocument.aspx?fileName=" + responseData);
                        }
                    } else {
                        alert(responseData.Value);
                    }
                });
            });
            $('#btnUploadFolder').on('click', function () {
                var data = new FormData();
                var file_uploaded = $("#folder_browser").get(0).files;
                $('.error-message').hide();
                if (file_uploaded.length == 0)
                {
                    $('.error-message').show();
                    $('.error-message').text("No file is present in folder.");
                    return;

                }
                for (var i = 0; i < file_uploaded.length; i++) {
                    data.append('data' + i, file_uploaded[i]);
                }

                $(".loader").show();
                $(".overlay").show();

                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: "/api/fileupload/UploadFolder",
                    contentType: false,
                    processData: false,
                    data: data
                });

                ajaxRequest.done(function (responseData, textStatus) {
                    if (textStatus == 'success') {
                        if (responseData != null) {
                            $('#ifrm-folder').attr('src', "ConvertFilesInFolder.aspx?folderName=" + responseData);
                        }
                    } else {
                        alert(responseData.Value);
                    }
                });
                ajaxRequest.error(function (requestObject, error, errorThrown)
                {
                    $('.error-message').show();
                    $('.error-message').text(requestObject.responseJSON.ExceptionMessage);

                });

            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <img src="images/loader1.gif" class="loader"/>
        <div class="overlay"></div>
        <table align="center">
            <tr>
                <td>
                    <table align="center" style="background-color: #9bb2c4">
                        <tr>
                            <td>
                                <label for="fileUpload1">Select file (.doc/.docx) to convert:</label>
                            </td>
                            <td>
                                <input class="custom-file-input file-text" type="file" id="fileUpload1" accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" />
                            </td>
                            <td>
                                <input type="button" value="Convert and Show" id="btnUploadFile" class="converter-button" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="fileUpload1">Select folder:</label>
                            </td>
                            <td>
                                <input type="file" id="folder_browser"  class="custom-file-input folder-text"
                                    accept=".doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" webkitdirectory directory multiple />
                            </td>
                            <td><input type="button" value="Convert and Download" id="btnUploadFolder" class="converter-button" /></td>

                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><span class="error-message" /></td>
            </tr>
        </table>
        
        <div style="margin: 11px;">
            <iframe style="width: 100%;height: 850px;" id="ifrm-document"></iframe>
            <iframe style="display:none" id="ifrm-folder"></iframe>
        </div>
    </div>
    </form>
</body>
</html>
