﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web_DocToTextConverter
{
    public partial class ConvertFilesInFolder : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string basePath = ConfigurationManager.AppSettings["BasePath"];
                string folderName = Request["folderName"];
                string folderPath = Path.Combine(basePath + folderName);
                ConvertDirectoryContents(folderPath);

                CreateAndDownloadZip(basePath + "converted_" + folderName);

                Directory.Delete(folderPath, true);

                Directory.Delete(basePath + "converted_" + folderName, true);

                HttpCookie myCookie = new HttpCookie("");
                myCookie["status"] = "Done";
                myCookie["status1"] = "Pending";
                Response.Cookies.Add(myCookie);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void CreateAndDownloadZip(string folderToBeZipped)
        {
            var downloadFileName = string.Format("ConvertedText-{0}.zip", DateTime.Now.ToString("yyyy-MM-dd-HH_mm_ss"));
            Response.ContentType = "application/zip";
            Response.AddHeader("Content-Disposition", "filename=" + downloadFileName);
            //Zip the contents of the selected files
            using (var zip = new ZipFile())
            {
                zip.AddDirectory(folderToBeZipped);

                // Send the contents of the ZIP back to the output stream
                zip.Save(Response.OutputStream);
            }
        }

        private void ConvertDirectoryContents(string dir)
        {
            try
            {
                string basePath = ConfigurationManager.AppSettings["BasePath"];
                foreach (string fileName in Directory.GetFiles(dir))
                {
                    // Create Destination Direciry
                    string destDirectory = dir.Replace(basePath, basePath + "\\converted_");
                    string destinationFileName = fileName.Replace(basePath, basePath + "\\converted_");
                    if (!Directory.Exists(destDirectory))
                    {
                        Directory.CreateDirectory(destDirectory);
                    }
                    destinationFileName = Regex.Replace(destinationFileName, @"(\.docx|\.doc)\b", ".txt", RegexOptions.IgnoreCase);

                    bool isConverted = MakeCallToWordCleanerExe(fileName, destinationFileName);
                }
                foreach (string d in Directory.GetDirectories(dir))
                {
                    ConvertDirectoryContents(d);
                }

            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static bool MakeCallToWordCleanerExe(string sourceFile, string destinationFile)
        {
            Process wordCleanerProc = new Process();
            wordCleanerProc.StartInfo.FileName = ConfigurationManager.AppSettings["WordCleanerConverterExe"];
            wordCleanerProc.StartInfo.Arguments = string.Format(" \"{0}\" \"{1}\"", sourceFile, destinationFile);

            wordCleanerProc.StartInfo.UseShellExecute = false;
            wordCleanerProc.StartInfo.RedirectStandardError = true;
            wordCleanerProc.Start();
            string errors = wordCleanerProc.StandardError.ReadToEnd();
            if (!errors.Equals(""))
            {
                throw new Exception(errors);
            }

            wordCleanerProc.WaitForExit();
            var exitCode = wordCleanerProc.ExitCode;
            wordCleanerProc.Close();

            return (exitCode != 1);
        }
    }
}